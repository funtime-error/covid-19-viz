# COVID-19 Global Confirmed Cases

[Demo](https://covid-19-viz.now.sh/)

Geospatial visualization of globally confirmed cases with [kepler.gl](https://kepler.gl/) and [React](https://reactjs.org/)

Data downloaded from [https://data.humdata.org/](https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases)
