const csv = require('csv-parser');
const fs = require('fs');
const moment = require('moment');

const data = {
  fields: [
    { name: 'country', format: '', type: 'string' },
    { name: 'state', format: '', type: 'string' },
    { name: 'day', format: 'YYYY-M-D H:m:s', type: 'timestamp' },
    { name: 'latitude', format: '', type: 'real' },
    { name: 'longitude', format: '', type: 'real' },
    { name: 'count', format: '', type: 'integer' }
  ],
  rows: []
};

fs.createReadStream('time_series_covid19_confirmed_global_narrow.csv')
  .pipe(csv(true))
  .on('data', row => {
    const country = `${row['Country/Region']}`;
    const state = `${row['Province/State']}`;
    const day = `${moment(row['Date']).format('YYYY-MM-DD HH:mm:ss ZZ')}`;
    const latitude = +row['Lat'];
    const longitude = +row['Long'];
    const count = +row['Value'];

    data.rows.push([country, state, day, latitude, longitude, count]);
    // data.rows.push(row);
  })
  .on('end', () => {
    console.log('done');

    fs.writeFile('covid19-data.json', JSON.stringify(data), err => {
      console.log(err);
    });
  })
  .on('error', error => {
    console.error(error);
  });
