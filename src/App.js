import KeplerGl from 'kepler.gl';
import { addDataToMap } from 'kepler.gl/actions';
import keplerGlReducer from 'kepler.gl/reducers';
import React from 'react';
import { taskMiddleware } from 'react-palm/tasks';
import { Provider, useDispatch } from 'react-redux';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import useSWR from 'swr';

const reducer = combineReducers({
  keplerGl: keplerGlReducer

  // <-- other reducers go here
});

const store = createStore(reducer, {}, applyMiddleware(taskMiddleware));

const fetcher = async url => await fetch(url).then(response => response.json());

const Map = props => {
  const dispatch = useDispatch();
  const { data } = useSWR(
    'https://gist.githubusercontent.com/funtime-error/46cffd491afeeff4fbbbf55d8ccf0a97/raw/6edbb00046ec089475a6fdf99bd17c199cb07db3/covid19-data.json',
    fetcher
  );

  React.useEffect(() => {
    if (data) {
      dispatch(
        addDataToMap({
          // datasets
          datasets: {
            info: {
              label: 'COVID-19 Global Confirmed Cases',
              id: 'covid19Data'
            },
            data
          },
          // option
          option: {
            centerMap: true,
            readOnly: false
          },
          // config
          config: {}
        })
      );
    }
  }, [dispatch, data]);

  return (
    <KeplerGl
      height={window.innerHeight}
      id="map"
      mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_API_KEY}
      width={window.innerWidth}
    />
  );
};

function App() {
  return (
    <Provider store={store}>
      <Map />
    </Provider>
  );
}

export default App;
